package com.automationanywhere.EPIC.commands.packages;

import com.automationanywhere.EPIC.commands.businesslogic.APIAuthorisation;
import com.automationanywhere.EPIC.commands.businesslogic.FHIR;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static org.apache.logging.log4j.LogManager.getLogger;

@BotCommand
@CommandPkg(name = "SessionStart",
        label = "Start EPIC Session" ,
        description = "Start EPIC Session",
        node_label = "Start Session {{sessionName}}",
        icon = "pkg.svg",
        comment = true
        )

public class APISession_Start {
    private static final Logger logger = getLogger();

    @Sessions
    private Map<String, Object> sessionMap;
    public void setSessionMap(Map<String, Object> sessionMap) {
        this.sessionMap = sessionMap;
    }

    @Execute
    public void execute(
            @Idx(index = "1", type = TEXT)
            @Pkg(label = "Session Name")
            @NotEmpty
                    String sessionName,
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "Url", description = "https://{hostname}/{instance}/ " +
                    "e.g. for EPIC sandbox https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token")
            @NotEmpty
                    String authUrl,
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "Issuer", description = "Issuer or Client Id")
            @NotEmpty
                    String issuer,
            @Idx(index = "4", type = TEXT)
            @Pkg(label = "PEM (Private Key) file path")
            @NotEmpty
                    String pemFilepath
    )throws ParseException, IOException, InvalidKeySpecException, NoSuchAlgorithmException, InterruptedException
    {
        String accessToken = "";
        if (!sessionMap.containsKey(sessionName)) {
            try {
                APIAuthorisation auth = new APIAuthorisation();
                //Generate the new JWT token.
                String jwtToken = auth.generateJWTToken(pemFilepath, issuer, authUrl);

                //Put the delay of some seconds, make this configurable.
                TimeUnit.SECONDS.sleep(1);

                //Generate the new access token.
                accessToken = auth.getToken(authUrl, jwtToken);

                FHIR fhir = new FHIR(authUrl, accessToken, issuer, pemFilepath);
                sessionMap.put(sessionName, fhir);
            } catch (Exception ex) {
                throw new BotCommandException("Exception message: " + ex.getMessage());
            }
        }
    }
}
