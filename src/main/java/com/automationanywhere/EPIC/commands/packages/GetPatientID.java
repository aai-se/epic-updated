package com.automationanywhere.EPIC.commands.packages;

import com.automationanywhere.EPIC.commands.businesslogic.FHIR;
import com.automationanywhere.EPIC.commands.businesslogic.Patient;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
import static org.apache.logging.log4j.LogManager.getLogger;

@BotCommand
@CommandPkg(name = "GetPatientID",
        label = "Get Patient ID" ,
        description = "Get the patient ID",
        node_label = "EPIC_Get_Patient_ID",
        icon = "pkg.svg",
        return_label = "Patient_ID",
        return_type = STRING,
        return_required = true)

public class GetPatientID {
    private static final Logger logger = getLogger();

    @Sessions
    private Map<String, Object> sessionMap;

    public void setSessionMap(Map<String, Object> sessionMap) {
        this.sessionMap = sessionMap;
    }

    @Execute
    public Value<?> execute(
            @Idx(index = "1", type = TEXT)
            @Pkg(label = "First Name")
            @NotEmpty
                    String firstName,
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "Family Name")
            @NotEmpty
                    String familyName,
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "Date of Birth in yyyy-MM-dd format ex: 2020-01-01")
            @NotEmpty
                    String dob,

            @Idx(index = "4", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "Default")
            @NotEmpty String sessionName
            ) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {

            Patient patient = new Patient(sessionMap);
            String patientID = patient.getPatientID(sessionName, firstName, familyName, dob);

            return new StringValue(patientID);
    }
}
