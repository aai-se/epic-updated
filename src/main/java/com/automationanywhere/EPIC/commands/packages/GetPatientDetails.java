package com.automationanywhere.EPIC.commands.packages;

import com.automationanywhere.EPIC.commands.businesslogic.Patient;
import com.automationanywhere.EPIC.commands.businesslogic.FHIR;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.DICTIONARY;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
import static org.apache.logging.log4j.LogManager.getLogger;

@BotCommand
@CommandPkg(name = "Get_Patient_ID_Dict", label = "Get_Patient_ID_Dict" , description = "EPIC-Get_Patient_ID", node_label = "EPIC_Get_Patient_ID", icon = "pkg.svg", return_label = "Get Details in Dictionary Format",return_type = DICTIONARY,return_sub_type =STRING ,return_required = true)

public class GetPatientDetails {
    private static final Logger logger = getLogger();

    @Sessions
    private Map<String, Object> sessionMap;

    public void setSessionMap(Map<String, Object> sessionMap) {
        this.sessionMap = sessionMap;
    }

    @Execute
    public DictionaryValue execute(
            @Idx(index = "1", type = TEXT)
            @Pkg(label = "First Name")
            @NotEmpty
                    String firstName,
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "Family Name")
            @NotEmpty
                    String familyName,
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "Date of Birth in yyyy-MM-dd format")
            @NotEmpty
                    String dob,
            @Idx(index = "4", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "Default")
            @NotEmpty String sessionName

    ) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        /*FHIR fhir = (FHIR) this.sessionMap.get(sessionName);
        String accessToken = fhir.getToken();

        Patient patient = new Patient();
        Map<String, Value> result = patient.getPatientDetails(firstName,familyName,dob,accessToken);*/

        Patient patient = new Patient(sessionMap);
        Map<String, Value> result = patient.getPatientDetails(sessionName, firstName, familyName, dob);

        return new DictionaryValue(result);
    }
}
