package com.automationanywhere.EPIC.commands.packages;

import com.automationanywhere.EPIC.commands.businesslogic.Patient;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
import static org.apache.logging.log4j.LogManager.getLogger;

@BotCommand
@CommandPkg(name = "ConditionSearch", label = "ConditionSearch" , description = "EPIC-ConditionSearch", node_label = "EPIC_ConditionSearch", icon = "pkg.svg", return_label = "Output",return_type = STRING,return_required =true)

public class ConditionSearch {
    private static final Logger logger = getLogger();

    @Sessions
    private Map<String, Object> sessionMap;
    public void setSessionMap(Map<String, Object> sessionMap) {
        this.sessionMap = sessionMap;
    }

    @Execute
    public Value<?> execute(
            @Idx(index = "1", type = TEXT)
            @Pkg(label = "Patient_ID")
            @NotEmpty
                    String patientID,
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "Enter the Problem Type")
            @NotEmpty
                    String pList,
            @Idx(index = "3", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "Default")
            @NotEmpty
                    String sessionName
            ) throws ParseException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {

        Patient patient = new Patient(sessionMap);
        String result = patient.getConditionSearch(sessionName,patientID,pList);

        return new StringValue(result);
    }
}
