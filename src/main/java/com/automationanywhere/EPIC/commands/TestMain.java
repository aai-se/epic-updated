package com.automationanywhere.EPIC.commands;

import com.automationanywhere.EPIC.commands.businesslogic.APIAuthorisation;
import com.automationanywhere.EPIC.commands.businesslogic.FHIR;
import com.automationanywhere.EPIC.commands.businesslogic.Patient;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class TestMain {

    public static void main(String[] args)
    {
        String sessionName = "session1";
        String pemFilepath = "C:\\Users\\pinkesh.achhodwala\\OneDrive - Automation Anywhere Software Private Limited\\IdeaProjects\\EPIC_v3\\AATestPvtKey1.pem";
        String issuer = "03bb6daa-df60-4a3a-8955-a531bf9bbc32";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token";
        Map<String, Object> sessionMap = new LinkedHashMap<>();

        try {
            //HashMap<String, Value> patientInfo = new LinkedHashMap<>();
            //patientInfo.put("1", new StringValue("Pinkesh"));

            APIAuthorisation auth = new APIAuthorisation();
            String jwtToken = auth.generateJWTToken(pemFilepath, issuer, Url);

            //Put the delay of some seconds, make this configurable.
            //TimeUnit.SECONDS.sleep(7);

            String accessToken = auth.getToken(Url, jwtToken);
            //String accessToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJ1cm46b2lkOmZoaXIiLCJjbGllbnRfaWQiOiIwM2JiNmRhYS1kZjYwLTRhM2EtODk1NS1hNTMxYmY5YmJjMzIiLCJlcGljLmVjaSI6InVybjplcGljOk9wZW4uRXBpYy1jdXJyZW50IiwiZXBpYy5tZXRhZGF0YSI6IjNfV091MWJKazFlMnloekRmN3lKVXhBZnZveWhPdHF3SWU1UjNKMjlYZkx5dXJIcGxLcXJtWXE4aU13R09WenpJM3cwT0NXNURiMkxiOWVlSWtTQWxfd1VYTzhvenBXTGpyMkFxVVZ0aXgySDRUdE94STZINGdtT1pCRDdTNkU3IiwiZXBpYy50b2tlbnR5cGUiOiJhY2Nlc3MiLCJleHAiOjE2MzcxMzM2MzQsImlhdCI6MTYzNzEzMDAzNCwiaXNzIjoidXJuOm9pZDpmaGlyIiwianRpIjoiZDBhNGFhNDYtZjgyYy00MjQyLThiODQtODYxZGY5NWYzNzk0IiwibmJmIjoxNjM3MTMwMDM0LCJzdWIiOiJleGZvNkU0RVhqV3NuaEExT0dWRWxndzMifQ.exfmOkVsQlZr42oGeGsTx1gmpWzPA_6ijK_Iy4511t-5kQkHnN4cfRj3yDaJyevdziPVCkgO8Df2qMdANQZp63xub4Qq2SAvOySb480ePkNoss5eo3hbuL-xFhN3WLlSI1s0zq8gWD3Xqn-H6oPppBU8PolUana0-3eW3W-a1Nr3rOYtWXfrh6k6s0m6-02K47sBc6wm8qFdSulKusaoB3VAHet-QnsoB3LRBpvLt6dGAQ7tsOx89KkCnaF7zk1LPoSDp9mC1ieOwRJMZo0o9FJPxrhdWCLwWNh-JjZAxdHeNzwTPYXxzMFT3zX08ZcQ8Tvqb6M-RVyntx52Yb1WdQ";
            //System.out.println("Very first time Access Token is: " + accessToken);

            FHIR fhir = new FHIR(Url, accessToken, issuer, pemFilepath);
            sessionMap.put(sessionName, fhir);

            //Test GetPatientID()
            //Call 1
            Patient patient = new Patient(sessionMap);
            String patientID = patient.getPatientID(sessionName, "Derrick", "Lin", "1973-06-03");

            fhir = (FHIR) sessionMap.get(sessionName);
            String newAccessToken1 = fhir.getToken();

            //System.out.println("Call 1 access token: " + newAccessToken1);
            System.out.println("Call 1 PatientID: " + patientID);

            /*
            //Call 2
            patient = new Patient(sessionMap);
            patientID = patient.getPatientID(sessionName, "Derrick", "Lin", "1973-06-03");

            fhir = (FHIR) sessionMap.get(sessionName);
            String newAccessToken2 = fhir.getToken();

            System.out.println("Call 2 access token: " + newAccessToken2);

            if(newAccessToken1.equals(newAccessToken2))
                System.out.println("Used old access token");
            else
                System.out.println("Generated the New token");

             */

            //Test GetPatientDetails()
            patient = new Patient(sessionMap);
            Map<String, Value> result = patient.getPatientDetails(sessionName, "Derrick", "Lin", "1973-06-03");

            System.out.println("Call 1 access token: " + result.get("PatientID").toString());
        }
        catch (Exception ex){
            System.out.println("Error: " + ex.getMessage());
        }
    }

}
