package com.automationanywhere.EPIC.commands.businesslogic;

public class FHIR {
    public FHIR() {}

    public FHIR(String Url, String Token){
        this.Url = Url;
        this.Token = Token;
    }

    public FHIR(String authUrl, String Token, String Issuer, String PemFilePath){
        this.authUrl = authUrl;
        this.Token = Token;
        this.Issuer = Issuer;
        this.PemFilePath = PemFilePath;
    }

    String Token = "";
    String Url = "";
    String authUrl = "";
    String Issuer = "";
    String PemFilePath = "";

    public String getIssuer() {return Issuer;}

    public String getToken() { return Token; }

    public String getURL() { return Url; }

    public String getAuthUrl() { return authUrl; }

    public String getPemFilePath() { return PemFilePath; }
}
