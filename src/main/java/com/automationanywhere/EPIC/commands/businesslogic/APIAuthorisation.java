package com.automationanywhere.EPIC.commands.businesslogic;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class APIAuthorisation {
    public APIAuthorisation(){}

    public String generateJWTToken(String filepath, String issuer, String audience) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        String jwtToken = "";

        try {
            File F = new File(filepath);
            PrivateKey privateKey = readPrivateKey(F);

            Instant now = Instant.now();
            String jwtIssuer = issuer;
            String jwtAud = audience;
            String jwtSubject = issuer; //subject;

            Date epochTime = new Date(Calendar.getInstance().getTimeInMillis() + (300*1000)); //300 seconds
            jwtToken = Jwts.builder()
                    .setAudience(jwtAud)
                    .setIssuedAt(Date.from(now))
                    .setExpiration(epochTime)
                    .setIssuer(jwtIssuer)
                    .setSubject(jwtSubject)
                    .setId(UUID.randomUUID().toString())
                    .signWith(privateKey, SignatureAlgorithm.RS384)
                    .signWith(privateKey)
                    .compact();
        }
        catch (Exception e) {
            jwtToken = e.getMessage();
        }

        return jwtToken;
    }

    public static RSAPrivateKey readPrivateKey(File file) throws Exception {
        String key = Files.readString(file.toPath());

        String privateKeyPEM = key
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PRIVATE KEY-----", "");

        byte[] encoded = Base64.decode(privateKeyPEM);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);

        return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
    }

    public String getToken(String Url, String jwtToken) throws IOException {
        String urlParameters = "grant_type=client_credentials&client_assertion_type=urn:ietf:params:oauth:client-assertion-type:jwt-bearer&client_assertion=" + jwtToken;
        String authToken = "";

        //URL url = new URL("https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token");
        URL url = new URL(Url);
        HttpURLConnection http = (HttpURLConnection) url.openConnection();
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;
        int responseCode6 = 0;

        http.setInstanceFollowRedirects(false);
        http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        http.setRequestProperty("charset", "utf-8");
        http.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        http.connect();
        //http.setUseCaches(false);

        try (OutputStream os = http.getOutputStream()) {
            os.write(postData);
            responseCode6 = http.getResponseCode();

            if (responseCode6 == HttpURLConnection.HTTP_OK) {
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(http.getInputStream(), StandardCharsets.UTF_8))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }

                    JSONObject jsonObject = new JSONObject(response.toString());
                    authToken = (String) jsonObject.get("access_token");

                    http.disconnect();

                }
            }
        } catch (Exception e) {
            authToken = "failure";
        }

        return authToken;
    }

}
