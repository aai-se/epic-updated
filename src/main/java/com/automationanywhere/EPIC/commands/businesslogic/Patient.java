package com.automationanywhere.EPIC.commands.businesslogic;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.LinkedHashMap;
import java.util.Map;

public class Patient {
    public Patient(Map<String, Object> sessionMap){ this.sessionMap = sessionMap;}

    FHIR fhir;
    Map<String, Object> sessionMap;

    public String getPatientID(String sessionName, String fname, String lname, String dob) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Patient?family="+lname+"&given="+fname+"&birthdate="+dob+"&_id=";

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "GET");

        try {
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // Success
                String inputLine = "";

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) response.append(inputLine);
                in.close();

                status = response.toString();
                JSONObject json = new JSONObject(status);
                JSONArray array = json.getJSONArray("entry");
                JSONObject element = (JSONObject) array.get(0);
                JSONObject resource = element.getJSONObject("resource");

                return resource.getString("id"); //return patient ID;
            }
            else
                status = "Invalid response received.";
        }
        catch(Exception e) {
            status = "Error occurred while fetching Patient ID: " + e.getMessage();
        }

        return status;
    }

    public Map<String, Value> getPatientDetails(String sessionName, String fname, String lname, String dob) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "";
        Map<String, Value> patientInfo = new LinkedHashMap();

        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Patient?family="+lname+"&given="+fname+"&birthdate="+dob+"&_id=";

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "GET");

        try {
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));

                String inputLine = "";
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                status = response.toString();
                JSONObject json = new JSONObject(status);
                JSONArray array = json.getJSONArray("entry");
                JSONObject element = (JSONObject) array.get(0);
                JSONObject resource = element.getJSONObject("resource");

                String patientId = resource.getString("id");
                String gender = resource.getString("gender");
                String dateOfBirth = resource.getString("birthDate");

                String maritalStatus = resource.getJSONObject("maritalStatus").getString("text");

                JSONArray name = resource.getJSONArray("name");
                JSONObject element1 = (JSONObject) name.get(0);
                String fullname = element1.getString("text");

                JSONArray phn = resource.getJSONArray("telecom");
                JSONObject element2 = (JSONObject) phn.get(0);
                String telephn = element2.getString("value");

                JSONArray address = resource.getJSONArray("address");
                JSONObject element3 = (JSONObject) address.get(0);
                String country = element3.getString("country");
                String city = element3.getString("city");

                patientInfo.put("PatientID", new StringValue(patientId));
                patientInfo.put("Name", new StringValue(fullname));
                patientInfo.put("Gender", new StringValue(gender));
                patientInfo.put("Date of Birth", new StringValue(dateOfBirth));
                patientInfo.put("Country", new StringValue(country));
                patientInfo.put("City", new StringValue(city));
                patientInfo.put("Marital Status", new StringValue(maritalStatus));
                patientInfo.put("Contact Number", new StringValue(telephn));
            }
            else
                patientInfo.put("Status", new StringValue("failure due to invalid response received."));
        }
        catch(Exception e){
            patientInfo.put("Status", new StringValue("Error occurred while fetching patient details: " + e.getMessage()));
        }

        return patientInfo;
    }

    public String getConditionSearch(String sessionName, String id, String plist) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Condition?patient="+id+"&category="+plist;

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "GET");

        try {
            //System.out.println(responseCode);
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in2 = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));

                String inputLine2 = "";
                StringBuilder response2 = new StringBuilder();

                while ((inputLine2 = in2.readLine()) != null) {
                    response2.append(inputLine2);
                }
                in2.close();

                status = response2.toString();
            }
            else
                status = "Invalid response received.";
        }
        catch(Exception e){
            status = "Error occurred while fetching condition search: " + e.getMessage();
        }

        return status;
    }

    public String confirmAppointmentDetails(String sessionName, String appid) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/STU3/Appointment/"+appid;

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "GET");

        try {
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in2 = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));

                String inputLine2 = "";
                StringBuilder response2 = new StringBuilder();
                while ((inputLine2 = in2.readLine()) != null) {
                    response2.append(inputLine2);
                }
                in2.close();

                status = response2.toString();
            }
            else
                status = "Invalid response received.";
        }
        catch(Exception e){
            status = "Error occurred while confirming appointment details: " + e.getMessage();
        }

        return status;
    }

    public String getDiagnosticReport(String sessionName, String id) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status="";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Condition?patient="+id+"&category=problem-list-item";

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "GET");

        try {
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in2 = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));

                String inputLine2 = "";
                StringBuilder response2 = new StringBuilder();
                while ((inputLine2 = in2.readLine()) != null) {
                    response2.append(inputLine2);
                }
                in2.close();

                status = response2.toString();
            }
            else
                status = "Invalid response received.";
        }
        catch(Exception e){
            status = "Error occurred while fetching diagnostic report: " + e.getMessage();
        }

        return status;
    }

    public String findAppointment(String sessionName) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "", json = "";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/STU3/Appointment/$find";

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "POST");
        conn.connect();

        byte[] postData = json.getBytes(StandardCharsets.UTF_8);
        try (OutputStream os = conn.getOutputStream()) {
            os.write(postData);

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }

                    status = response.toString();
                }
            }
            else
                status = "Invalid response received.";
        } catch (Exception e) {
            status = "Error occurred while finding the appointment: " + e.getMessage();
        }

        return status;
    }

    public String findContraindication(String sessionName, String pid) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Condition?patient="+pid+"&category=encounter-diagnosis";

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "GET");

        try {
            //System.out.println(responseCode);
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in2 = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));

                String inputLine2 = "";
                StringBuilder response2 = new StringBuilder();
                while ((inputLine2 = in2.readLine()) != null) {
                    response2.append(inputLine2);
                }
                in2.close();

                status = response2.toString();
            }
            else
                status = "Invalid response received.";
        }
        catch(Exception e){
            status = "Error occurred while finding contraindication: " + e.getMessage();
        }

        return status;
    }

    public String getMedicationRequest(String sessionName, String id) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/MedicationRequest?patient=" + id + "&status=active";

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "GET");

        try {
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in3 = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));

                String inputLine3 = "";
                StringBuilder response3 = new StringBuilder();
                while ((inputLine3 = in3.readLine()) != null) {
                    response3.append(inputLine3);
                }
                in3.close();

                status = response3.toString();
            }
            else
                status = "Invalid response received.";
        }
        catch(Exception e){
            status = "Error occurred while fetching the medication request: " + e.getMessage();
        }

        return status;
    }

    public String getObservation(String sessionName, String id) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Observation?patient=" + id + "&category=laboratory";

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "GET");

        try {
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in2 = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                String inputLine2 = "";
                StringBuilder response2 = new StringBuilder();

                while ((inputLine2 = in2.readLine()) != null) {
                    response2.append(inputLine2);
                }
                in2.close();

                status = response2.toString();
            }
            else
                status = "Invalid response received.";
        }
        catch(Exception e){
            status = "Error occurred while fetching observation: " + e.getMessage();
        }

        return status;
    }

    public String procedureCoverage(String sessionName, String id, String patientStatus) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/coverage?patient=" + id + "&status="+ patientStatus;

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "GET");

        try {
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in3 = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));

                String inputLine3 = "";
                StringBuilder response3 = new StringBuilder();
                while ((inputLine3 = in3.readLine()) != null) {
                    response3.append(inputLine3);
                }
                in3.close();

                status = response3.toString();
            }
            else
                status = "Invalid response received.";
        }
        catch(Exception e){
            status = "Error occurred while procedure coverage: " + e.getMessage();
        }

        return status;
    }

    public String getProcedure(String sessionName, String id) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Procedure?patient="+id;

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "GET");

        try {
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in2 = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));

                String inputLine2 = "";
                StringBuilder response2 = new StringBuilder();
                while ((inputLine2 = in2.readLine()) != null) {
                    response2.append(inputLine2);
                }
                in2.close();

                status = response2.toString();
            }
            else
                status = "Invalid response received.";
        }
        catch(Exception e){
            status = "Error occurred while fetching the procedure: " + e.getMessage();
        }

        return status;
    }

    public String scheduleAppointment(String sessionName, String pid, String appid) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "";
        String json="{\"resourceType\":\"Parameters\",\"parameter\":[{\"name\":\"patient\",\"valueIdentifier\":{\"value\":\""+pid+"\"}},{\"name\":\"appointment\",\"valueIdentifier\":{\"value\":\""+appid+"\"}},{\"name\":\"appointmentNote\",\"valueString\":\"Note text containing info related to the appointment.\"}]}";
        byte[] postData = json.getBytes(StandardCharsets.UTF_8);


        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/STU3/Appointment/$book";
        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "POST");
        conn.connect();

        try (OutputStream os = conn.getOutputStream()) {
            os.write(postData);
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }

                    status = response.toString();
                }
            }else
                status = "Invalid response received.";
        }
        catch(Exception e){
            status = "Error occurred while scheduling the appointment: " + e.getMessage();
        }

        return status;
    }

    public String getHistory(String sessionName, String pid) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        String status = "";
        String Url = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Observation?patient="+pid+"&category=social-history";

        fhir = (FHIR) sessionMap.get(sessionName);
        HttpURLConnection conn = Utility.getHttpUrlConnection(Url, fhir, "GET");

        try {
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in2 = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                String inputLine2 = "";
                StringBuilder response2 = new StringBuilder();

                while ((inputLine2 = in2.readLine()) != null) {
                    response2.append(inputLine2);
                }
                in2.close();

                status = response2.toString();
            }
            else
                status = "Invalid response received.";
        }
        catch(Exception e){
            status = "Error occurred while fetching the history: " + e.getMessage();
        }

        return status;
    }

}
