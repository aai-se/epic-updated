package com.automationanywhere.EPIC.commands.businesslogic;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Utility {
    //public Utility(FHIR fhir){ this.fhir = fhir;}

    FHIR fhir;
    public static HttpURLConnection getHttpUrlConnection(String Url, FHIR fhir, String method) throws IllegalStateException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
        HttpURLConnection conn;

        if(method == "GET") {
            try {
                URL obj = new URL(Url);
                conn = (HttpURLConnection) obj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", "Bearer " + fhir.getToken());

                if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
                    return conn;
                }
                else {
                    try {
                        String accessToken = RegenerateAccessToken(fhir);
                        //Update the FHIR and thus SessionMap variables with latest Access token value.
                        fhir.Token = accessToken;

                        obj = new URL(Url);
                        conn = (HttpURLConnection) obj.openConnection();
                        conn.setRequestMethod("GET");
                        conn.setRequestProperty("Content-Type", "application/json");
                        conn.setRequestProperty("Authorization", "Bearer " + accessToken);

                        return conn;
                    } catch (Exception ex) {
                        throw ex;
                    }
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }
        else if(method == "POST") {
            try {
                URL obj = new URL(Url);
                conn = (HttpURLConnection) obj.openConnection();
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);

                conn.setInstanceFollowRedirects(false);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", "Bearer " + fhir.getToken());

                if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
                    return conn;
                }
                else {
                    try {
                        String accessToken = RegenerateAccessToken(fhir);
                        fhir.Token = accessToken;

                        obj = new URL(Url);
                        conn = (HttpURLConnection) obj.openConnection();
                        conn.setRequestMethod("POST");
                        conn.setDoOutput(true);

                        conn.setInstanceFollowRedirects(false);
                        conn.setRequestProperty("Content-Type", "application/json");
                        conn.setRequestProperty("Authorization", "Bearer " + accessToken);

                    } catch (Exception ex) {
                        throw ex;
                    }
                }

                return conn;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        return null;
    }

    public static String RegenerateAccessToken(FHIR fhir) throws IOException, InterruptedException, NoSuchAlgorithmException, InvalidKeySpecException {
        APIAuthorisation auth = new APIAuthorisation();
        String jwtToken = auth.generateJWTToken(fhir.getPemFilePath(), fhir.getIssuer(), fhir.getAuthUrl());

        //Put the delay of some seconds, make this configurable.
        TimeUnit.SECONDS.sleep(1);

        return auth.getToken(fhir.getAuthUrl(), jwtToken);
    }

}
