package com.automationanywhere.EPIC.commands.packages;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class APISession_StartCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(APISession_StartCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    APISession_Start command = new APISession_Start();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(!(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("authUrl") && parameters.get("authUrl") != null && parameters.get("authUrl").get() != null) {
      convertedParameters.put("authUrl", parameters.get("authUrl").get());
      if(!(convertedParameters.get("authUrl") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","authUrl", "String", parameters.get("authUrl").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("authUrl") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","authUrl"));
    }

    if(parameters.containsKey("issuer") && parameters.get("issuer") != null && parameters.get("issuer").get() != null) {
      convertedParameters.put("issuer", parameters.get("issuer").get());
      if(!(convertedParameters.get("issuer") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","issuer", "String", parameters.get("issuer").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("issuer") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","issuer"));
    }

    if(parameters.containsKey("pemFilepath") && parameters.get("pemFilepath") != null && parameters.get("pemFilepath").get() != null) {
      convertedParameters.put("pemFilepath", parameters.get("pemFilepath").get());
      if(!(convertedParameters.get("pemFilepath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","pemFilepath", "String", parameters.get("pemFilepath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("pemFilepath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","pemFilepath"));
    }

    command.setSessionMap(sessionMap);
    try {
      command.execute((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("authUrl"),(String)convertedParameters.get("issuer"),(String)convertedParameters.get("pemFilepath"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","execute"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
